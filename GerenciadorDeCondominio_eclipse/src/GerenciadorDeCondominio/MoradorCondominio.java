package GerenciadorDeCondominio;

/**
 * Este objeto representa um morador e suas respectivas funções, que são adicionar, alterar e excluir
 * @version 1.0 2017-11-09
 * @author otavio rocha
 * @author paulo jaco
 * @author juliano fabri
 *
 */
public class MoradorCondominio {
	private String nomeMorador;
	private String telefoneMorador;
	private String emailMorador;
	private boolean representanteApartamento;
	private int numeroApartamentoVincular;
	private Controlador control;
	private int ativo;
	
	public MoradorCondominio(String nome, String telefone, String email) 
	{
		nomeMorador = nome;
		telefoneMorador = telefone;
		emailMorador = email;
		ativo = 1;
	}
	
	/**
	 *  Associa o morador a um apartamento e define se ele é ou não representante do mesmo
	 *  @param representante	indica se o morador é representante do apartamento ou não, sendo 1 para quando for representante e 0 para quando não for representante
	 *  @param numeroApartamento	indica o número do apartamento a ser associado ao morador
	 */
	public void incluirMorador(boolean representante, int numeroApartamento){

		this.representanteApartamento = representante;
		this.numeroApartamentoVincular = numeroApartamento;
		
	}

	
	/**
	 *  Remove logicamente o morador
	 */
	public void excluirMorador(){
		ativo = 0;
	}

	
	/**
	 *  altera as informações do morador
	 *  @param nome	novo nome do morador a ser alterado
	 *  @param telefone	novo telefone do morador a ser alterado
	 *  @param representante	indica se o morador é representante do apartamento ou não, sendo 1 para quando for representante e 0 para quando não for representante
	 *  @param numeroApartamento	indica o novo número do apartamento a ser associado ao morador	
	 */
	public void alterarMorador(String nome, String telefone, boolean representante, int numeroApartamento) 
	{
		nomeMorador = nome;
		telefoneMorador = telefone;
		representanteApartamento = representante;
		numeroApartamentoVincular = numeroApartamento;
	}

}