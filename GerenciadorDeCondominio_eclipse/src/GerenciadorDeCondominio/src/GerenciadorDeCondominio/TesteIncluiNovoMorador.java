package GerenciadorDeCondominio.src.GerenciadorDeCondominio;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


import static org.junit.Assert.*;

import org.junit.Test;

public class TesteIncluiNovoMorador {
	
	static  String nome ;
	static String telefone;
	static String email;
	static boolean representante;
	static int numeroApartamento;
	
	private Controlador controle;
	
	

	@BeforeClass
	public static void configuraTeste(){
		nome = "Otavio";
		telefone = "35991943061";
		email = "otavio@gmail.com";
		representante = true;
		numeroApartamento = 22;
	}
	
	@Before
	public void configura(){
		controle.incluiNovoMorador(nome, telefone, email);
	}
	
	@Test
	public void incluiMorador1(){
		controle.associaApartamentoAoMorador(representante, numeroApartamento);
	}
	
	@After
	public void desconfigura(){
		controle = null;
	}
	
	@AfterClass
	public static void desconfiguraTeste(){
		nome = null;
		telefone = null;
		email = null;
	}

}
