package GerenciadorDeCondominio;

/**
 * controlador que serve como intermediario para a view e a classe Morador
 * @version 1.0 2017-11-09
 * @author otavio rocha
 * @author paulo jaco
 * @author juliano fabri
 *
 */
public class Controller {
	
	private Morador morador;
	
	public Controller() {
		
	}

	/**
	 * 
	 * @param nome	nome do novo morador
	 * @param telefone	telefone do novo morador
	 * @param email	email do novo morador
	 */
	public void incluiNovoMorador(String nome, String telefone, String email){
            morador =  new Morador(nome, email, telefone);
	}
        
	
	/**
	 *  associa um morador a um numero de apartamento
	 *  @param representante	indica se o morador é representante do apartamento ou não, sendo 1 para quando for representante e 0 para quando não for representante
	 *  @param numeroApartamento	indica o número do apartamento a ser associado ao morador
	 */
        public void associaApartamentoAoMorador(boolean representante, int numeroApartamento){
        	morador.incluirMorador(representante, numeroApartamento);
        }
        
        
        /**
         * altera as informações do morador
         * 
         * @param nome	nome do novo morador
         * @param telefone	telefone do novo morador
         * @param representante	indica se o morador é representante do apartamento ou não, sendo 1 para quando for representante e 0 para quando não for representante
         * @param numeroApartamento	indica o número do apartamento a ser associado ao morador
         */
        public void alteraMoradorExistente(String nome, String telefone, boolean representante, int numeroApartamento){
            morador.alterarMorador(nome, telefone, representante, numeroApartamento);
        }
        
        /**
         * 
         */
        public void excluiMoradorExistente(){
            morador.excluirMorador();
        }

}